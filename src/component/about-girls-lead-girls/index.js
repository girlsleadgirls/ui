import React from 'react'
import { Timeline, Row, Col, Typography, Divider } from 'antd';
import logo from '../../img/logo.png';
import aboutFooter from '../../img/about-footer.jpg';
import './style.css';

const AboutGirlsLeadGirls = () => {
  const { Paragraph } = Typography;
  return (
    <>
      <Row gutter={24}>
        <Col span={12}>
          <img src={logo} alt="About Girls Lead Girls" srcset={logo} className="about-footer" />
        </Col>
        <Col span={12}>
          <Paragraph className="justify-content">Puducherry has a highest number of education institutions in the country. The All India Survey of Higher Education (2011-12) reveals that the population of Puducherry includes of 67,123 eligible college-age (18-23 years) girls (Puducherry natives), but that only 23,576 (35%) girl students (which includes girl students native to other states or foreign countries) were studying in 4 universities/institute of national importance and 83 colleges in Puducherry. A very limited number of girls have access to higher education, and even those girls that do are not feeling safe in the institution and on their way to the colleges. These unsafe situations make them dropout from the college and keep them more vulnerable.</Paragraph>
          <Paragraph className="justify-content">Trust for Youth and Child Leadership (TYCL) has conducted online and in-person enquiries to identify the top five issues faced by college girls in Puducherry. The following ten responses were received from the girls and people working with college girls.</Paragraph>
        </Col>
      </Row>
      <Divider />
      <Row gutter={24}>
        <Col span={12}>
          <Timeline>
            <Timeline.Item>Insecure to travel alone/fear of going alone.</Timeline.Item>
            <Timeline.Item>Fear of stalking and harassment at anywhere and anytime.</Timeline.Item>
            <Timeline.Item>Fear of dowry which pushes the girl’s parent into burden (Causing mental health issues), 4. Forced marriages/Fear of getting married soon without achieving her dream.</Timeline.Item>
            <Timeline.Item>Denial/Restriction to undergo higher studies outside of Puducherry</Timeline.Item>
            <Timeline.Item>Getting love proposals from strangers/Hard to trust any men even male teachers</Timeline.Item>
            <Timeline.Item>Wrong calls/text from strange people (online/offline).</Timeline.Item>
            <Timeline.Item>Lack of trust/Misunderstanding from parent’s side, if girls talk to any boys (Due to social status and stigma).</Timeline.Item>
            <Timeline.Item>No proper career and higher education guidance, and</Timeline.Item>
            <Timeline.Item>Denial of extra-curricular activities</Timeline.Item>
          </Timeline>
          <h3>Aim Objectives:</h3>
          <Paragraph>The Girls lead Girls aims to ensure safe and inclusive space for all the school and college going girls in Puducherry.</Paragraph>
          <Paragraph>1. To develop a comprehensive (Physical, emotional and knowledge) curriculum to build young girls as strong self-defender.</Paragraph>
          <Paragraph>2. To prepare college girls as agent of change for girl’s self-defence.</Paragraph>
          <Paragraph>3. To equip school girls to tackle violence (physical, emotional, sexual, online and knowledge) by themselves.</Paragraph>
          <Paragraph>4. To document girl’s self-defence mechanisms and create knowledge depository of it.</Paragraph>
        </Col>
        <Col span={12}>
          <img src={aboutFooter} alt="About Girls Lead Girls" srcset={aboutFooter} className="about-footer" />
        </Col>
      </Row>
    </>
  )
}

export default AboutGirlsLeadGirls;