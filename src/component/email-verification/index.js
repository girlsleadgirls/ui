import React, { useEffect } from 'react'
import { notification } from 'antd';
import { withRouter } from 'react-router-dom';
import { action } from '../../store';
import { validateToken } from '../../actions/Authentication-action';
import PanelSpinner from '../user/PanelSpinner';

const EmailVerification = ({ isEmailVerified, match: { params: { token } }, loading }) => {
  useEffect(() => {
    if (token) {
      action(
        validateToken({
          token
        })
      );
    }
  }, []);

  if (isEmailVerified) {
    notification["success"]({
      message: "Email Verified Successfully"
    });
  }
  return (
    <PanelSpinner />
  )
}

export default withRouter(EmailVerification); 