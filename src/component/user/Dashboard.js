import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { Modal, Progress, Card, Typography, Input } from 'antd';
import { saveFeedback } from '../../actions/Authentication-action';

import { getChapterStatus } from '../../actions/User-action';
import { action } from '../../store';
import { CheckCircleFilled, CloseCircleFilled } from '@ant-design/icons';
import PanelSpinner from "./PanelSpinner";

const { Title } = Typography;
const { TextArea } = Input;

const selectedCSS = {
    position: "absolute",
    top: "50%",
    transform: `translate( -50%, -50% )`, right: "10px", color: "#87d068", fontSize: "20px"
};
const rejectedCSS = { position: "absolute", transform: `translate( -50%, -50% )`, top: "50%", right: "10px", color: "red", fontSize: "20px" };

const Dashboard = ({ chapters, isPreAssessmentDone, userId, isAllChapterDone, isPostAssessmentDone, profile, loading }) => {
    const [state, setState] = useState({
        progress: 0,
        isModalVisible: false,
        feedback: profile.feedback
    })

    const { progress, isModalVisible, feedback } = state;

    const getProgress = () => {
        if (!chapters) {
            return 0
        }
        let totalNumber = chapters.length + 1;
        let completedChapters = chapters.filter((chapter) => {
            return chapter.isDone == true
        })
        let completedNumber = (completedChapters && completedChapters.length) ? completedChapters.length : 0;
        if (profile && profile.feedback) {
            completedNumber++;
        }
        let progress = 0;
        if (completedNumber > 0) {
            progress = ((parseInt(completedNumber) / parseInt(totalNumber)) * 100);
            progress = Math.round(progress);
        }
        return progress
    }
    useEffect(() => {
        if (chapters) {
            setState({ ...state, progress: getProgress() });
        }
    }, [chapters])
    useEffect(() => {
        action(getChapterStatus());
    }, [])
    useEffect(() => {
        if (profile) {
            setState({
                ...state,
                isModalVisible: false,
                feedback: profile.feedback,
                progress: getProgress()
            })
        }
    }, [profile])

    const showModal = () => {
        setState({ ...state, isModalVisible: true });
    };

    const handleOk = () => {
        function callback() {
            setState({ ...state, isModalVisible: false, progress: getProgress() });
        }
        let payload = {
            data: {
                id: profile.user_id,
                feedback: feedback
            }
        }
        action(saveFeedback(payload))
    };

    const handleCancel = () => {
        setState({ ...state, isModalVisible: false });
    };
    return (
        <>
            {
                loading && (<PanelSpinner></PanelSpinner>)
            }
            <div className="site-layout-background" style={{ padding: 24 }}>
                <Title level={4} type="secondary">Your progress on GirlsLeadGirls program!!</Title>
                <Progress percent={progress} status="active" style={{ marginTop: "10px" }} />
            </div>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 260, marginTop: 15 }}>
                <Card title="Card Title" className="segmenets">
                    <p className="segment-desc">The complete segment of GirlsLeadGirls program listed here.</p>
                    {
                        (chapters || []).map((item, key) => (
                            item.key == "preAssessment" ? (
                                <Card.Grid className="lesson" style={{ position: "relative" }}>
                                    { item.isDone && <CheckCircleFilled style={selectedCSS} />}
                                    <Link to="/lessons">{item.name}</Link>
                                </Card.Grid>
                            ) :
                                item.key == "postAssessment" ? (
                                    <Card.Grid className="lesson" style={{ position: "relative" }}>
                                        { isAllChapterDone && item.isDone && <CheckCircleFilled style={selectedCSS} />}
                                        {
                                            isPreAssessmentDone && isAllChapterDone ? (<Link to="/lessons/post-assessment" style={{ cursor: "pointer", color: "#40a9ff", padding: "25px 0", margin: "-25px 0" }}>{item.name}</Link>)
                                                : item.name
                                        }
                                    </Card.Grid>
                                ) : (
                                        <Card.Grid key={key} className="lesson" style={{ position: "relative" }}>
                                            { item.isDone && item.isPassed ? <CheckCircleFilled style={selectedCSS} /> : item.isDone ? <CloseCircleFilled style={rejectedCSS} /> : ""}
                                            {

                                                isPreAssessmentDone ? (<Link to={'/lessons/chapter/' + item.key} style={{ cursor: "pointer", color: "#40a9ff", padding: "25px 0", margin: "-25px 0" }}>{item.name}</Link>)
                                                    : item.name
                                            }
                                        </Card.Grid>

                                    )
                        ))
                    }
                    <Card.Grid className="lesson" style={{ position: "relative" }}>
                        {isPostAssessmentDone && feedback && <CheckCircleFilled style={selectedCSS} />}
                        {
                            isPostAssessmentDone ? (<div onClick={showModal} style={{ cursor: "pointer", color: "#40a9ff", padding: "25px 0", margin: "-25px 0" }}> Feedback</div>)
                                : "Feedback"
                        }
                    </Card.Grid>
                    <Card.Grid className="lesson">
                        {
                            progress == 100 ? (<Link to={'/lessons/generate-certificate/' + userId} style={{ cursor: "pointer", color: "#40a9ff", padding: "25px 0", margin: "-25px 0" }}> Generate Certificate</Link>)
                                : "Generate Certificate"
                        }
                    </Card.Grid>
                </Card>
            </div>
            <Modal title="Feedback Form" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} okText="Submit" >
                <p>Give your feedback about GirlsLeadGirls Program in few words which will give us to improve the service.</p>

                {
                    profile && <TextArea rows={7} value={feedback} onChange={(e) => { setState({ ...state, feedback: e.target.value }) }} />
                }
            </Modal>
        </>
    )
}
export default Dashboard