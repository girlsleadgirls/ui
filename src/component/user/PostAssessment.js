import React, { useState, useEffect } from 'react';
import { Button, Typography } from 'antd';
import { ArrowLeftOutlined, ArrowRightOutlined, SendOutlined } from '@ant-design/icons';
import AssessmentQuestion from "./AssessmentQuestion";
import PanelSpinner from "./PanelSpinner";

import { getPostAssessmentQuestions, insertPostAssessmentQuestions } from '../../actions/Assessment-action';
import { action } from '../../store';
const { Title } = Typography;

const PostAssessment = ({ postAssessmentQuestions, loading, inCompletePostAssessment, isPostAssessmentDone, correctPostAssessmentAnswers, userId, profileInfo }) => {
    useEffect(() => {
        // console.log(getPreAssessmentQuestions, "getPreAssessmentQuestions()");
        action(getPostAssessmentQuestions({ userId }));
    }, [])
    useEffect(() => {
        if (postAssessmentQuestions.length > 0) {
            let myState = { ...state, questions: postAssessmentQuestions }
            if (isPostAssessmentDone) {
                myState.layout = 2
            }
            setState(myState);
        }
    }, [postAssessmentQuestions, postAssessmentQuestions]);

    const [state, setState] = useState({
        layout: 0,
        activeQuestion: 0,
        questions: [
            {
                id: 1,
                desc: "How do you defend 1 ?",
                answer: 1
            },
            {
                id: 2,
                desc: "How do you defend 2 ?",
                answer: 1
            },
            {
                id: 3,
                desc: "How do you defend 3 ?",
                answer: 1
            },
            {
                id: 4,
                desc: "How do you defend 4 ?",
                answer: 1
            }
        ]
    });
    const { layout, questions, activeQuestion } = state;
    const changeLayout = (stage) => {
        setState({ ...state, layout: stage });
    }
    const changeQuestion = (changeIndex) => {
        let actQtn = activeQuestion + changeIndex
        setState({ ...state, activeQuestion: actQtn });
    }
    const updateQuestion = (question, index) => {
        let qtns = questions
        qtns[index] = question
        setState({ ...state, questions: qtns });
        action(insertPostAssessmentQuestions({
            user_id: userId,
            question_id: question.id,
            score: question.answer,
            assessment_type: "post"
        }))
    }
    return (
        <>
            {
                loading && (<PanelSpinner></PanelSpinner>)
            }
            <Title level={5} style={{ textAlign: 'center', backgroundColor: '#87d068', textTransform: "uppercase", margin: "-24px -24px 24px -24px", padding: "5px 0", color: "white" }}>
                Post-Assessment
                 {
                    questions && questions.length && layout == 1 && (
                        <span style={{ float: "right", display: "inline-block", padding: "0 10px 0 10px" }}>{activeQuestion + 1}/{questions.length}</span>
                    )
                }
            </Title>
            {
                layout == 0 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    <div>
                        Hello <strong>{profileInfo && profileInfo.name}</strong>,<br />
                        You have successfuly all segments of GirlsLeadGirls program. Finally, you will be having post assessment which has<strong> {questions && questions.length} questions</strong> with<strong> multiple choices</strong>. You have to answer this question in a single session. If you have disconnected, you supposed retake the assessment.
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <Button onClick={() => { changeLayout(1) }} type="primary" size="large" >
                            {
                                inCompletePostAssessment ? "Resume Assessment " : "Start Assessment "
                            }
                            <ArrowRightOutlined /> </Button>
                    </div>
                </div>
            }
            {
                layout == 1 &&
                <div style={{ maxWidth: 900, margin: "0 auto" }}>
                    {
                        questions &&
                        questions.map((question, index) => (
                            activeQuestion == index && <AssessmentQuestion updateQuestion={updateQuestion} question={question} key={index} index={index} />
                        ))
                    }
                    <div style={{ marginTop: 24 }}>
                        {
                            activeQuestion > 0 &&
                            <Button onClick={() => { changeQuestion(-1) }} style={{ minWidth: 100 }} size="large" type="danger"><ArrowLeftOutlined /> Previous  </Button>
                        }
                        {
                            activeQuestion < questions.length - 1 &&
                            < Button onClick={() => { changeQuestion(1) }} style={{ minWidth: 100, float: "right" }} type="primary" size="large" disabled={questions && questions[activeQuestion] && questions[activeQuestion].answer ? false : true}>Next <ArrowRightOutlined /> </Button>
                        }
                        {
                            activeQuestion == questions.length - 1 &&
                            <Button onClick={() => { changeLayout(2) }} style={{ minWidth: 100, float: "right", backgroundColor: '#87d068', color: "white" }} size="large" disabled={questions && questions[activeQuestion] && questions[activeQuestion].answer ? false : true} >Submit Assessment <SendOutlined /> </Button>
                        }
                    </div>
                </div>
            }
            {
                layout == 2 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    <div>
                        Congratulations <strong>{profileInfo && profileInfo.name}</strong>!<br />
                        You have successfuly completed the post-assessment of GirlsLeadGirls program. We believe this program has strengthem your defeding skills in all aspects as a women to lead the society. And Please generate your certificate for this program.
                        You have given <strong>{correctPostAssessmentAnswers} correct answers out of {questions.length}</strong> questions.
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <Button href="/user/lessons/generate-certificate" type="primary" size="large" >Generate Certificate <ArrowRightOutlined /> </Button>
                    </div>
                </div>
            }

        </>
    )
}

export default PostAssessment