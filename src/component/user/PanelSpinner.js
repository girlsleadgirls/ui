import { Spin } from 'antd';
import React, { useState } from "react";

const PanelSpinner = () => {
    return (
        <>
            <div style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                background: "white",
                left: "0px",
                top: "0px",
                zIndex: "9999999"

            }}>
                <Spin tip="Loading..." style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: `translate( -50%, -50% )`,
                }}></Spin>
            </div>
        </>
    )
}
export default PanelSpinner