import React, { useState, useEffect } from 'react';
import { Button, Typography } from 'antd';
import { ArrowLeftOutlined, ArrowRightOutlined, SendOutlined } from '@ant-design/icons';
import AssessmentQuestion from "./AssessmentQuestion";
import PanelSpinner from "./PanelSpinner";

import { getPreAssessmentQuestions, insertPreAssessmentQuestions } from '../../actions/Assessment-action';
import { action } from '../../store';

const { Title } = Typography;

const PreAssement = ({ preAssessmentQuestions, loading, inCompletePreAssessment, isPreAssessmentDone, userId, correctPreAssessmentAnswers, profileInfo }) => {

    const [state, setState] = useState({
        layout: 0,
        activeQuestion: 0,
        questions: preAssessmentQuestions
    });
    const changeLayout = (stage) => {
        setState({ ...state, layout: stage });
    }
    const changeQuestion = (changeIndex) => {
        let actQtn = activeQuestion + changeIndex
        setState({ ...state, activeQuestion: actQtn });
    }
    const updateQuestion = (question, index) => {
        let qtns = questions
        qtns[index] = question
        setState({ ...state, questions: qtns });
        action(insertPreAssessmentQuestions({
            user_id: userId,
            question_id: question.id,
            score: question.answer,
            assessment_type: "pre"
        }))
    }
    const { layout, questions, activeQuestion } = state;

    useEffect(() => {
        // console.log(getPreAssessmentQuestions, "getPreAssessmentQuestions()");
        action(getPreAssessmentQuestions({ userId }));
    }, [])
    useEffect(() => {
        if (preAssessmentQuestions.length > 0) {
            let myState = { ...state, questions: preAssessmentQuestions }
            if (isPreAssessmentDone) {
                myState.layout = 2
            }
            setState(myState);
        }
    }, [preAssessmentQuestions, isPreAssessmentDone]);

    return (
        <>
            {
                loading && (<PanelSpinner></PanelSpinner>)
            }
            <Title level={5} style={{ textAlign: 'center', backgroundColor: '#87d068', textTransform: "uppercase", margin: "-24px -24px 24px -24px", padding: "5px 0", color: "white" }}>
                Pre-Assessment
                {
                    questions && questions.length && layout == 1 && (
                        <span style={{ float: "right", display: "inline-block", padding: "0 10px 0 10px" }}>{activeQuestion + 1}/{questions.length}</span>
                    )
                }
            </Title>

            {
                layout == 0 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    <div>
                        Hello <strong>{profileInfo && profileInfo.name}</strong>,<br />
                        Let's start with GirlsLeadGirls program. This pre-assement session will have <strong>{questions && questions.length} questions</strong> with<strong> multiple choices</strong>. You have to answer this question in a single session. If you have disconnected, you supposed retake the assessment.
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <Button onClick={() => { changeLayout(1) }} type="primary" size="large" >
                            {
                                inCompletePreAssessment ? "Resume Assessment " : "Start Assessment "
                            }

                            <ArrowRightOutlined /> </Button>
                    </div>
                </div>
            }
            {
                layout == 1 &&
                <div style={{ maxWidth: 900, margin: "0 auto" }}>
                    {
                        questions &&
                        questions.map((question, index) => (
                            activeQuestion == index && <AssessmentQuestion updateQuestion={updateQuestion} question={question} key={index} index={index} />
                        ))
                    }
                    <div style={{ marginTop: 24 }}>
                        {
                            activeQuestion > 0 &&
                            <Button onClick={() => { changeQuestion(-1) }} style={{ minWidth: 100 }} size="large" type="danger"><ArrowLeftOutlined /> Previous  </Button>
                        }
                        {
                            activeQuestion < questions.length - 1 &&
                            < Button onClick={() => { changeQuestion(1) }} style={{ minWidth: 100, float: "right" }} type="primary" size="large" disabled={questions && questions[activeQuestion] && questions[activeQuestion].answer ? false : true}  >Next <ArrowRightOutlined /> </Button>
                        }
                        {
                            activeQuestion == questions.length - 1 &&
                            <Button onClick={() => { changeLayout(2) }} style={{ minWidth: 100, float: "right", backgroundColor: '#87d068', color: "white" }} size="large" disabled={questions && questions[activeQuestion] && questions[activeQuestion].answer ? false : true}  >Submit Assessment <SendOutlined /> </Button>
                        }
                    </div>
                </div>
            }
            {
                layout == 2 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    <div>
                        Congratulations <strong>{profileInfo && profileInfo.name}</strong>!<br />
                        You have successfuly completed the pre-assessment of GirlsLeadGirls program. Let's get into the Segments.
                        You have given <strong>{correctPreAssessmentAnswers} correct answers out of {questions.length}</strong> questions.
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <Button href="/user" type="primary" size="large" >Continue <ArrowRightOutlined /> </Button>
                    </div>
                </div>
            }

        </>
    )
}

export default PreAssement
