import React from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import PreAssessment from './PreAssessment'
import PostAssessment from './PostAssessment'
import Chapter from "./Chapter";
import GenerateCertificate from "./GenerateCertificate";
import { connect } from 'react-redux';

const Lesson = ({ userDetail }) => {
    return (
        <>
            <Router basename="/user/lessons">
                <div className="site-layout-background" style={{ padding: 24, float: "left", width: "100%", position: "relative" }}>
                    <Switch>
                        <Route path="/post-assessment">
                            <PostAssessment profileInfo={userDetail.profile} postAssessmentQuestions={userDetail.postAssessmentQuestions} correctPostAssessmentAnswers={userDetail.correctPostAssessmentAnswers} userId={userDetail.userId} loading={userDetail.loading} inCompletePostAssessment={userDetail.inCompletePostAssessment} isPostAssessmentDone={userDetail.isPostAssessmentDone} />
                        </Route>
                        <Route path="/chapter/:id">
                            <Chapter profileInfo={userDetail.profile} chapterDetail={userDetail.chapterDetail} userId={userDetail.userId} loading={userDetail.loading} />
                        </Route>
                        <Route path="/generate-certificate/:id">
                            <GenerateCertificate />
                        </Route>
                        <Route path="/">
                            <PreAssessment profileInfo={userDetail.profile} preAssessmentQuestions={userDetail.preAssessmentQuestions} correctPreAssessmentAnswers={userDetail.correctPreAssessmentAnswers} userId={userDetail.userId} loading={userDetail.loading} inCompletePreAssessment={userDetail.inCompletePreAssessment} isPreAssessmentDone={userDetail.isPreAssessmentDone} isPreAssessmentDone={userDetail.isPreAssessmentDone} />
                        </Route>
                    </Switch>
                </div>
            </Router>

        </>
    )
}
const mapStateToProps = (state) => {
    const { UserReducer } = state;
    return { userDetail: UserReducer }
}

export default connect(mapStateToProps)(Lesson)
