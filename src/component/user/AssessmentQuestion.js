import React, { useState } from "react";
import { Radio, Typography } from 'antd';
const radioStyle = {
    display: 'block',
    height: '35px',
    lineHeight: '35px',
};
const { Title } = Typography;

const AssessmentQuestions = (props) => {
    const [state, setState] = useState({ value: props.question.answer, answers: props.question.choices });
    const onChange = e => {
        setState({
            ...state,
            value: e.target.value,
        });
        let qtn = props.question;
        qtn.answer = e.target.value
        props.updateQuestion(qtn, props.index)
    };

    const { value, answers } = state
    return (
        <div>
            <Title level={4} style={{ marginBottom: 20 }}>{props.question.desc}</Title>
            <div style={{ paddingBottom: 20 }}>
                <Radio.Group onChange={onChange} value={value}>
                    {
                        answers && answers.map((answer, index) => (
                            <Radio style={radioStyle} value={answer.id} key={index}>
                                {answer.desc}
                            </Radio>
                        ))
                    }
                </Radio.Group>
            </div>

        </div>
    )
}
export default AssessmentQuestions