import React, { useState, useEffect } from 'react';
import { Form, Input, Select, Button, DatePicker, Typography } from 'antd';
import { UserOutlined, KeyOutlined, MailOutlined, MobileOutlined, TeamOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { action } from '../../store';
import { updateUser } from '../../actions/Authentication-action';
import moment from 'moment';

const { Title } = Typography;
const leftInput = { display: 'inline-block', width: 'calc(50% - 8px)' };
const rightInput = { display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' };
const fullInput = { display: 'inline-block', width: 'calc(100% - 8px)' }
const { Option } = Select;

const Profile = ({ userDetail }) => {

    const [form] = Form.useForm();
    const onFinish = values => {
        values.monthly_family_income = parseInt(values.monthly_family_income);
        values.dob = values.dob.format("YYYY-MM-DD");
        values.email = userDetail.email;
        values.user_id = userDetail.user_id;
        action(updateUser(values));
        // console.log('Success:', values);
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    useEffect(() => {
        console.log((new Date(userDetail.dob) == "Invalid Date"), "userDetail")
        form.setFieldsValue({
            username: userDetail.name || '',
            phone: userDetail.contact || '',
            email: userDetail.email || '',
            dob: moment(userDetail.dob) || '',
            city: userDetail.city || '',
            region: userDetail.religion || '',
            edu_qualification: userDetail.educational_qualification || '',
            edu_insititution: userDetail.educational_institution || '',
            fathers_employment: userDetail.fathers_employment || '',
            mothers_employment: userDetail.mothers_employment || '',
            monthly_family_income: userDetail.monthly_family_income || '',
            no_of_siblings: userDetail.no_of_siblings || '',
            religion: userDetail.religion || '',
            Community: userDetail.community || '',
            how_do_you_know: userDetail.how_do_you_know || '',
            remember: true
        });

        // if (!(new Date(userDetail.dob) == "Invalid Date")) {
        //     form.setFieldsValue({
        //         dob: new Date(userDetail.dob)
        //     })
        // }

    }, [userDetail.name]);

    // console.log(initialValues, "initialValues");
    return (
        <>
            <div className="site-layout-background" style={{ padding: 24 }}>
                <Title level={5} style={{ textAlign: 'center', backgroundColor: '#87d068', textTransform: "uppercase", margin: "-24px -24px 24px -24px", padding: "5px 0", color: "white" }}>Your Profile Information</Title>
                <Form
                    name="basic"
                    // initialValues={initialValues}
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item>
                        <Input.Group compact>
                            <Form.Item
                                name="username"
                                rules={[{ required: true, message: 'Please input your username!' }]}
                                style={leftInput}
                            >
                                <Input placeholder="UserName" prefix={<UserOutlined />} />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[{ disabled: true }]}
                                disabled={true}
                                style={rightInput}
                            >
                                <Input.Password disabled={true} prefix={<KeyOutlined />} placeholder="*****" />
                            </Form.Item>
                        </Input.Group>

                        <Input.Group >
                            <Form.Item
                                name="email"
                                rules={[{ required: true, message: 'Please input your email address!' }]}
                                style={fullInput}
                            >
                                <Input disabled={true} prefix={<MailOutlined />} placeholder="Email Address" />
                            </Form.Item>
                        </Input.Group>
                        <Input.Group compact>
                            <Form.Item
                                name="phone"
                                rules={[{ required: true, message: 'Please input your phone number!' }]}
                                style={leftInput}
                            >
                                <Input prefix={<MobileOutlined />} placeholder="Phone Number" />
                            </Form.Item>
                            <Form.Item
                                name="dob"
                                rules={[{ required: true, message: 'Please input your date of birth!' }]}
                                style={rightInput}
                            >
                                <DatePicker placeholder="Date of Birth" />
                            </Form.Item>
                        </Input.Group>
                        <Input.Group compact>
                            <Form.Item
                                name="city"
                                rules={[{ required: true, message: 'Please input your city!' }]}
                                style={leftInput}
                            >
                                <Input placeholder="City" />
                            </Form.Item>
                            <Form.Item
                                name="region"
                                rules={[{ required: true, message: 'Please select your region!' }]}
                                style={rightInput}
                            >
                                <Select placeholder="Region">
                                    <Option value="Rural">Rural</Option>
                                    <Option value="Sub Urban">Sub Urban</Option>
                                    <Option value="Urban">Urban</Option>
                                </Select>
                            </Form.Item>
                        </Input.Group>
                        <Input.Group compact>
                            <Form.Item
                                name="edu_qualification"
                                rules={[{ required: true, message: 'Please input your education qualification!' }]}
                                style={leftInput}
                            >
                                <Input placeholder="Education" prefix={<UserOutlined />} />
                            </Form.Item>
                            <Form.Item
                                name="edu_insititution"
                                rules={[{ required: true, message: 'Please input your education institution!' }]}
                                style={rightInput}
                            >
                                <Input placeholder="Institution" />
                            </Form.Item>
                        </Input.Group>
                        <Input.Group compact>
                            <Form.Item
                                name="fathers_employment"
                                rules={[{ required: true, message: "Please input your father's employement!" }]}
                                style={leftInput}
                            >
                                <Input placeholder="Father's Employment" />
                            </Form.Item>
                            <Form.Item
                                name="mothers_employment"
                                rules={[{ required: true, message: "Please input your mother's employment!" }]}
                                style={rightInput}
                            >
                                <Input placeholder="Mother's Employment" />
                            </Form.Item>
                        </Input.Group>
                        <Input.Group>
                            <Form.Item
                                name="monthly_family_income"
                                rules={[{ required: true, message: 'Please input your monthly family income!' }]}
                                style={fullInput}
                            >
                                <Input placeholder="Monthly Family Income" />
                            </Form.Item>
                        </Input.Group>
                        <Input.Group>
                            <Form.Item
                                name="no_of_siblings"
                                rules={[{ required: true, message: 'Please enter no. of siblings!' }]}
                                style={fullInput}
                            >
                                <Input prefix={<TeamOutlined />} placeholder="No. of Siblings" />
                            </Form.Item>
                        </Input.Group>
                        <Input.Group compact>
                            <Form.Item
                                name="religion"
                                rules={[{ required: true, message: 'Please input your religion!' }]}
                                style={leftInput}
                            >
                                <Input placeholder="Religion" />
                            </Form.Item>
                            <Form.Item
                                name="Community"
                                rules={[{ required: true, message: 'Please select your community!' }]}
                                style={rightInput}
                            >
                                <Select placeholder="Community">
                                    <Option value="SC">SC</Option>
                                    <Option value="ST">ST</Option>
                                    <Option value="MBC">MBC</Option>
                                    <Option value="OBC">OBC</Option>
                                    <Option value="Other">Other</Option>
                                </Select>
                            </Form.Item>
                        </Input.Group>
                        <Input.Group>
                            <Form.Item
                                name="how_do_you_know"
                                rules={[{ required: true, message: 'Please tell us how do you know about GirlsleadGrils!' }]}
                                style={fullInput}
                            >
                                <Input prefix={<QuestionCircleOutlined />} placeholder="How do you know about GirlsLeadGirls" />
                            </Form.Item>
                        </Input.Group>

                    </Form.Item>


                    <Form.Item className="text-center">
                        <Button type="primary" htmlType="submit" className="submit-cta" >
                            Update Profile
                            </Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    )
}
export default Profile