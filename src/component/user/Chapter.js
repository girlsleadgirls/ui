import React, { useState, useEffect } from 'react';
// import videoURL from '../../assest/YRC.mp4'
import { withRouter } from "react-router-dom"
import { Button, Typography } from 'antd';
import { ArrowLeftOutlined, ArrowRightOutlined, SendOutlined, CheckCircleOutlined, BackwardOutlined, ForwardOutlined, CaretRightOutlined, PauseOutlined, UndoOutlined, FullscreenOutlined } from '@ant-design/icons';
import AssessmentQuestion from "./AssessmentQuestion";
import PanelSpinner from "./PanelSpinner";
import "./Chapter.scss";
import { getChapterDetail, insertChapterQuestions, insertVideoTime } from '../../actions/User-action';

// const videoURL = "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4";
import { action } from '../../store';

const Chapter = ({ chapterDetail, loading, match, userId, profileInfo }) => {
    const [state, setState] = useState({
        layout: 0,
        playCTA: true,
        isVideoPaused: false,
        isVideoCompleted: false,
        activeQuestion: 0,
        runningTime: "00:00",
        totalDuration: "00:00",
        questions: chapterDetail.questions,
        title: chapterDetail.chapter_name,
        facilitator: chapterDetail.facilitator,
        videoURL: chapterDetail.video_url,
        lastSeen: chapterDetail.last_seen
    })
    const changeQuestion = (changeIndex) => {
        let actQtn = activeQuestion + changeIndex
        setState({ ...state, activeQuestion: actQtn });
    }
    const updateQuestion = (question, index) => {
        let qtns = questions
        qtns[index] = question
        setState({ ...state, questions: qtns });
        action(insertChapterQuestions({
            user_id: userId,
            question_id: question.id,
            score: question.answer,
            chapter_id: match.params.id
        }))
    }
    const { layout, playCTA, isVideoPaused, isVideoCompleted, questions, activeQuestion, title, facilitator, videoURL, runningTime, totalDuration, lastSeen } = state;
    const changeLayout = (stage) => {
        if (stage == 5) {
            let chapter_id = match.params.id
            let user_id = userId;
            action(getChapterDetail({ chapter_id, user_id }));
        }
        setState({ ...state, layout: stage });
        setTimeout(() => {
            if (stage == 1) {
                var video = document.getElementById("video");
                video.currentTime = lastSeen || 0;
            }
        })
    }

    const updateVideoTime = () => {
        var video = document.getElementById("video");
        var curmins = Math.floor(video.currentTime / 60);
        var cursecs = Math.floor(video.currentTime - curmins * 60);
        var durmins = Math.floor(video.duration / 60);
        var dursecs = Math.floor(video.duration - durmins * 60);
        if (cursecs < 10) { cursecs = "0" + cursecs; }
        if (dursecs < 10) { dursecs = "0" + dursecs; }
        if (curmins < 10) { curmins = "0" + curmins; }
        if (durmins < 10) { durmins = "0" + durmins; }
        setState({ ...state, runningTime: curmins + ":" + cursecs, totalDuration: durmins + ":" + dursecs })
        if (curmins && parseInt(curmins) > 0 && parseFloat(cursecs) == 0 && parseFloat(curmins) % 1 == 0) {
            console.log(curmins, parseFloat(cursecs))
            action(insertVideoTime({
                "user_id": userId,
                "chapter_id": match.params.id,
                "last_duration": video.currentTime,
                "is_done": false
            }))
        }
    }
    const playVideo = (e) => {
        let video = document.getElementById("video");
        setState({ ...state, playCTA: false, lastSeen: chapterDetail.isFailed || chapterDetail.isPassed ? 0 : lastSeen })
        let payload = {
            "user_id": userId,
            "chapter_id": match.params.id,
            "last_duration": video.duration,
            "is_done": false
        }
        video.onended = function () {
            setState({ ...state, playCTA: false, layout: 2, isVideoCompleted: true })
            payload.is_done = true;
            action(insertVideoTime(payload))
        };
        action(insertVideoTime(payload))
        video.currentTime = lastSeen || 0;
        video.play()
    }
    const pauseVideo = () => {
        let video = document.getElementById("video");
        video.pause()
        setState({ ...state, isVideoPaused: true })
    }
    const resumeVideo = () => {
        let video = document.getElementById("video");
        video.play()
        setState({ ...state, isVideoPaused: false })
    }
    const replayVideo = () => {
        let video = document.getElementById("video");
        video.pause()
        video.currentTime = 0
        video.play()
        setState({ ...state, isVideoPaused: false })
    }
    const rewindVideo = () => {
        let video = document.getElementById("video");
        video.pause()
        video.currentTime = video.currentTime - 6
        video.play()
        setState({ ...state, isVideoPaused: false })
    }
    const forwardVideo = () => {
        let video = document.getElementById("video");
        video.pause()
        video.currentTime = video.currentTime + 6
        video.play()
        setState({ ...state, isVideoPaused: false })
    }
    const fullScreen = () => {
        let video = document.getElementById("video");
        video.pause()
        video.play()
        video.requestFullscreen();
    }
    const submitAnswers = () => {
        let payload = {
            questions: [],
            callback: () => {
                changeLayout(5)
            }
        }
        questions.map((qtn) => {
            payload.questions.push({
                question_id: qtn.id,
                chapter_id: match.params.id,
                user_id: userId,
                score: qtn.answer
            })
        })
        action(insertChapterQuestions(payload))
    }
    const retake = () => {
        setState({ ...state, activeQuestion: 0, layout: 1 })
    }
    const { Title } = Typography;
    useEffect(() => {
        let chapter_id = match.params.id
        let user_id = userId;
        action(getChapterDetail({ chapter_id, user_id }));
    }, [])
    useEffect(() => {
        if (chapterDetail) {
            let myState = {
                ...state, questions: chapterDetail.questions, title: chapterDetail.chapter_name, facilitator: chapterDetail.facilitator,
                videoURL: chapterDetail.video_url, lastSeen: chapterDetail.last_seen
            }
            if (chapterDetail.isPassed) {
                myState.layout = 5
            }
            setState(myState);
        }
    }, [chapterDetail]);
    return (
        <div className="chapter-page" >
            {
                loading && (<PanelSpinner></PanelSpinner>)
            }
            <Title level={5} style={{ textAlign: 'center', backgroundColor: '#87d068', textTransform: "uppercase", margin: "-24px -24px 24px -24px", padding: "5px 0", color: "white" }}>
                {title}
                {
                    questions && questions.length && layout == 4 && (
                        <span style={{ float: "right", display: "inline-block", padding: "0 10px 0 10px" }}>{activeQuestion + 1}/{questions.length}</span>
                    )
                }
                {
                    layout == 1 && !playCTA && (
                        <span style={{ float: "right", display: "inline-block", padding: "0 10px 0 10px" }}>
                            {runningTime}/{totalDuration}
                        </span>
                    )
                }
            </Title>
            {
                layout == 0 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    {
                        chapterDetail.isFailed && questions ? (
                            <div>
                                Hello <strong>{profileInfo && profileInfo.name}</strong>,<br />
                                You have given <strong>{chapterDetail.correctAnswers} correct answers out of {questions.length}</strong> questions. Please retake the assessment.
                                <strong> {title}</strong> section has two parts, a Video Session and followed by an Assessment. This session was facilitated by <strong>{facilitator}</strong>. You should take the video session fully. If video is stopped anywhere, you have to start from begining.
                            </div>
                        ) :
                            (
                                <div>
                                    Hello <strong>{profileInfo && profileInfo.name}</strong>,<br />
                                    <strong> {title}</strong> section has two parts, a Video Session and followed by an Assessment. This session was facilitated by <strong>{facilitator}</strong>. You should take the video session fully. If video is stopped anywhere, you have to start from begining.
                                </div>
                            )
                    }
                    <div style={{ marginTop: 24 }}>
                        {
                            chapterDetail.isFailed ? (
                                <Button onClick={() => { changeLayout(1) }} type="danger" size="large" >
                                    Retake Lesson
                                    <ArrowRightOutlined />
                                </Button>
                            ) : (
                                    <Button onClick={() => { changeLayout(1) }} type="primary" size="large" >
                                        {chapterDetail && lastSeen > 0 ? "Resume Lesson " : "Start Lesson "}
                                        <ArrowRightOutlined />
                                    </Button>
                                )
                        }

                    </div>
                </div>
            }
            {
                layout == 1 &&
                <div style={{ position: "relative" }}>
                    {
                        playCTA && (<Button className="video-btn" onClick={playVideo} type="primary" size="large" ><CaretRightOutlined /> </Button>)
                    }
                    <video id="video" style={{ width: "100%", objectFit: "fill" }} poster="" onTimeUpdate={updateVideoTime}>
                        <source type="video/mp4" src={videoURL}></source>
                    </video>
                    {
                        !playCTA && (
                            <div className="video-control">
                                <Button type="secondary" onClick={rewindVideo} ><BackwardOutlined /></Button>
                                <Button type="secondary" onClick={pauseVideo}><PauseOutlined /></Button>
                                <Button onClick={resumeVideo} ><CaretRightOutlined /></Button>
                                <Button type="secondary" onClick={replayVideo}><UndoOutlined /></Button>
                                <Button type="secondary" onClick={fullScreen} ><FullscreenOutlined /></Button>
                                <Button type="secondary" onClick={forwardVideo} ><ForwardOutlined /></Button>
                                { isVideoCompleted && (<Button type="secondary" onClick={() => { changeLayout(2) }} style={{ backgroundColor: '#87d068', color: "white" }}> Take Assessment <ArrowRightOutlined /> </Button>)}
                            </div>
                        )
                    }
                </div>
            }
            {
                layout == 2 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    <div>
                        Congratulation <strong>{profileInfo && profileInfo.name}</strong>!<br />
                        You have successfully completed {title} chapter of GirlsLeadGirls. Hope, it will be great learning for you to defend any attack on you.
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <Button onClick={() => { changeLayout(1) }} type="primary" size="large" style={{ marginRight: "3px" }} danger><ArrowLeftOutlined /> Watch Again    </Button>
                        <Button onClick={() => { changeLayout(3) }} type="primary" size="large" style={{ backgroundColor: '#87d068', color: "white" }}>Continue   <ArrowRightOutlined /> </Button>
                    </div>
                </div>
            }
            {
                layout == 3 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    <div>
                        Let's take the assessment for {title}. It has {questions.length} multiple choice questions. If connection is lost, you have to start from Video. All the best
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <Button onClick={() => { changeLayout(4) }} type="primary" size="large" >Start Assessment <ArrowRightOutlined /> </Button>
                    </div>
                </div>
            }
            {
                layout == 4 &&
                <div style={{ maxWidth: 900, margin: "0 auto" }}>
                    {
                        questions &&
                        questions.map((question, index) => (
                            activeQuestion == index && <AssessmentQuestion updateQuestion={updateQuestion} question={question} key={index} index={index} />
                        ))
                    }
                    <div style={{ marginTop: 24 }}>
                        {
                            activeQuestion > 0 &&
                            <Button onClick={() => { changeQuestion(-1) }} style={{ minWidth: 100 }} size="large" type="danger"><ArrowLeftOutlined /> Previous  </Button>
                        }
                        {
                            activeQuestion < questions.length - 1 &&
                            < Button onClick={() => { changeQuestion(1) }} style={{ minWidth: 100, float: "right" }} type="primary" size="large" disabled={questions && questions[activeQuestion] && questions[activeQuestion].answer ? false : true} >Next <ArrowRightOutlined /> </Button>
                        }
                        {
                            activeQuestion == questions.length - 1 &&
                            <Button onClick={() => { changeLayout(5) }} style={{ minWidth: 100, float: "right", backgroundColor: '#87d068', color: "white" }} size="large" disabled={questions && questions[activeQuestion] && questions[activeQuestion].answer ? false : true} >Submit Assessment <SendOutlined /> </Button>
                        }
                    </div>
                </div>
            }
            {
                layout == 5 &&
                <div style={{ maxWidth: 800, margin: "0 auto", fontSize: 20, textAlign: 'center' }}>
                    {
                        chapterDetail.isPassed ? (
                            <div>
                                Congratulations <strong>{profileInfo && profileInfo.name}</strong>!<br />
                                You have successfuly completed physical defence course of GirlsLeadGirls program. Let's get into the next Segment.
                                You have given <strong>{chapterDetail.correctAnswers} correct answers out of {questions.length}</strong> questions.
                            </div>
                        ) : (
                                <div>
                                    Sorry <strong>{profileInfo && profileInfo.name}</strong>!<br />
                                You have given <strong>{chapterDetail.correctAnswers} correct answers out of {questions.length}</strong> questions. You need to learn more. So please retake the session.
                                </div>
                            )
                    }
                    {
                        chapterDetail.isPassed ? (
                            <div style={{ marginTop: 24 }}>
                                <Button href="/user/" type="primary" size="large" >Continue <ArrowRightOutlined /> </Button>
                            </div>

                        ) : (
                                <div style={{ marginTop: 24 }}>
                                    <Button onClick={() => { retake() }} type="danger" size="large" >Retake Lesson <ArrowRightOutlined /> </Button>
                                </div>
                            )
                    }

                </div>
            }
        </div >
    )
}
export default withRouter(Chapter) 