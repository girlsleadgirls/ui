import React from "react";
import { Page, Text, View, Image, Document, StyleSheet, PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import { withRouter } from "react-router-dom"
import border from '../../img/border.png'
import "./GenerateCertificate.scss"
// Create styles
const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        backgroundColor: "white"
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    }
});

const GenerateCertificate = ({ match: { params: { id } } }) => {
    const PDFDoc = () => {
        return (
            <Document  >
                <Page size="A4" style={styles.page} className="page-full" style={{ width: "100%", height: "400px", display: "block" }}>
                    <View style={styles.section}>
                        <Text>Section #{id} </Text>
                    </View>
                    <View style={styles.section}>
                        <Text>Section #2</Text>
                    </View>
                </Page>
            </Document>
        )
    }
    return (
        <div>
            {/* <PDFViewer style={{ width: "100%", height: "400px" }}><PDFDoc /></PDFViewer> */}
            <PDFDoc />
            {/* <PDFDownloadLink document={<PDFDoc />} fileName="somename.pdf">
                {({ blob, url, loading, error }) => (loading ? 'Loading document...' : 'Download now!')}
            </PDFDownloadLink> */}
        </div>
    )
}
export default withRouter(GenerateCertificate) 