import React, { useState } from 'react';
import { Layout, Menu } from 'antd';
import logo from '../../img/logo.png'
import {
  Link, useHistory
} from "react-router-dom";
import './layout.scss';

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

const GuestLayout = ({ children }) => {
  let history = useHistory();
  const { pathname } = history.location;

  const checkActiveClass = (url) => (pathname === url ? 'ant-menu-item-selected' : '');
  const currentMenu = ['/', "/about-girls-lead-girls", "/start-learning", "/enquiry"].indexOf(pathname);

  const [state, setstate] = useState(currentMenu)
  const updateMenuItem = ({ key }) => {
    setstate(key)
  }
  return (
    <Layout className="guest-container">
      <Header className="header">
        <div className="logo">
          <img src={logo} alt="" />
        </div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[state]} style={{ float: 'right' }} onClick={updateMenuItem}>
          <Menu.Item key="1" className={checkActiveClass('/')}>
            <Link to="/">Home</Link>
          </Menu.Item>
          <Menu.Item key="2" className={checkActiveClass('/about-girls-lead-girls')}>
            <Link to="/about-girls-lead-girls">About GirlsLeadGirls</Link>
          </Menu.Item>
          <Menu.Item key="3" className={checkActiveClass('/start-learning')}>
            <Link to="/start-learning/login">Start Learning</Link>
          </Menu.Item>
          <Menu.Item key="4" className={checkActiveClass('/enquiry')}>
            <Link to="/enquiry">Enquiry</Link>
          </Menu.Item>
        </Menu>

      </Header>
      <Content>
        <Layout className="site-layout-background" style={{ padding: '24px' }}>
          <Content style={{ padding: '0', minHeight: 500 }}>
            {children}
          </Content>
        </Layout>
      </Content>
      <Footer style={{ textAlign: 'center' }}>GirlsLeadGirls ©2020 Created by TYCL</Footer>
    </Layout>
  )
}

export default GuestLayout;