import React from 'react'
import { Menu } from 'antd';
import {
  Link
} from "react-router-dom";

const SideMenu = () => {
  return (
    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']} style={{ float: 'right' }}>
      <Menu.Item key="1">
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item key="2">
        <Link to="/about-girls-lead-girls">About GirlsLeadGirls</Link>
      </Menu.Item>
      <Menu.Item key="3">
        <Link to="/start-learning">Start Learning</Link>
      </Menu.Item>
      <Menu.Item key="4">
        <Link to="/enquiry">Enquiry</Link>
      </Menu.Item>
    </Menu>
  )
}

export default SideMenu;