import React, { useState } from 'react';
import { Layout, Menu, Avatar, Typography } from 'antd';
import { Link } from "react-router-dom";
import {
  PieChartOutlined,
  QuestionCircleOutlined,
  SolutionOutlined,
  UserOutlined,
} from '@ant-design/icons';
import logo from '../../img/logo.png'
import './UserLayout.scss';

const { Header, Content, Footer, Sider, } = Layout;
const { Title } = Typography;

const UserLayout = ({ children, profile }) => {
  const [state, setState] = useState({
    collapsed: false,
  });

  const onCollapse = collapsed => {
    console.log(collapsed);
    setState({ collapsed });
  };

  const { collapsed } = state;

  return (
    <Layout style={{ minHeight: '100vh' }} className="user-container">
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        {!collapsed && <div className="logo" > <img src={logo} alt="" /></div>}
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1" icon={<PieChartOutlined />}>
            <Link to="/">Dashboard</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<SolutionOutlined />}>
            <Link to="/lessons">Resume Your Course</Link>
          </Menu.Item>
          <Menu.Item key="3" icon={<UserOutlined />}>
            <Link to="/profile">Profile</Link>
          </Menu.Item>
          <Menu.Item key="4" icon={<QuestionCircleOutlined />}>
            <Link to="/query">Query</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" >
          <Title level={3} className="header-title">Hello {profile && profile.name}...!</Title>
          <Avatar size="large" style={{ backgroundColor: '#87d068', float: 'right', marginTop: "12px" }} icon={<UserOutlined />} />
          {<div className="logo" > <img src={logo} alt="" /></div>}
        </Header>
        <Content style={{ margin: '16px' }}>
          {children}
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    </Layout>
  )
}

export default UserLayout
