import React, { useEffect } from 'react';
import { Row, Col, Card } from 'antd';
import './dashboard.css';
import { Carousel } from 'antd';
import CourseList from '../courseList';
import { ShareAltOutlined } from '@ant-design/icons'

import { getChapters } from '../../actions/Guest-action';
import { action } from '../../store';
import { connect } from 'react-redux';

const Dashboard = ({ chapters }) => {
  const onChange = (a, b, c) => {
    console.log(a, b, c);
  }

  const gridStyle = {
    width: '50%',
    textAlign: 'center',
  };

  useEffect(() => {
    action(getChapters());
  }, []);

  return (
    <Row>
      <Col span={24}>
        <Carousel afterChange={onChange}>
          <div className="slider slider1" />
          <div className="slider slider2" />
          <div className="slider slider3" />
          <div className="slider slider4" />
        </Carousel>
      </Col>
      <Col span={24}>
        <Card title="Over View" style={{ textAlign: 'center' }}>
          {
            (chapters || []).map((item, key) => (
              <Card.Grid key={key} style={gridStyle}>
                {item.Chapter}
              </Card.Grid>
            ))
          }
        </Card>
      </Col>
    </Row>
  )
}

const mapStateToProps = (state) => {
  const { GuestReducer } = state;
  return { chapters: GuestReducer.chapterList }
}

export default connect(mapStateToProps)(Dashboard);