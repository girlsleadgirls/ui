import React, { useEffect } from "react";
import { Form, Input, Row, Button, Col, Card, notification } from 'antd';
import { insertEnquiry } from "../../actions/Guest-action";
import { action } from "../../store";
import { connect } from "react-redux";
import { resetAllStatus } from "../../actions/Authentication-action";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

const Enquiry = ({ guest: { isEnquirySubmitted } }) => {
  const [form] = Form.useForm();
  const onFinish = (values, e) => {
    console.log(values, e)
    action(
      insertEnquiry({
        ...values
      })
    );
    console.log(values);
  };

  useEffect(() => {

    if (isEnquirySubmitted) {
      notification["success"]({
        message: "Query Submitted Successfully"
      });

      action(
        resetAllStatus()
      );
      form.resetFields();
    }
  }, [isEnquirySubmitted]);

  return (
    <Row align="center">
      <Col span={12}>
        <Card title="Enquiry">
          <Row>
            <Col span={20}>
              {/* <h1></h1> */}
              <Form {...layout} form={form} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                <Form.Item name={['user', 'email']} label="Email" rules={[{ type: 'email' }, { required: true }]}>
                  <Input />
                </Form.Item>
                <Form.Item name={['user', 'title']} label="Title" rules={[{ required: true }]}>
                  <Input />
                </Form.Item>
                <Form.Item name={['user', 'message']} label="Message" rules={[{ required: true }]}>
                  <Input.TextArea />
                </Form.Item>
                <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                  <Button type="primary" htmlType="submit">
                    Submit
            </Button>
                </Form.Item>
              </Form>
            </Col>
            {/* <Col span={2}></Col> */}
          </Row>
        </Card>

      </Col>
    </Row>

  );
};

const mapStateToProps = (state) => {
  const { GuestReducer } = state;
  return { guest: GuestReducer }
}

export default connect(mapStateToProps)(Enquiry)