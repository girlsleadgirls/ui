import React, { memo, useEffect } from 'react';
import { Form, Input, Select, Button, Tabs, DatePicker, notification, Row, Col, Card } from 'antd';
import './startLearning.css';
import { UserOutlined, KeyOutlined, MailOutlined, MobileOutlined, TeamOutlined, QuestionCircleOutlined, LockOutlined } from '@ant-design/icons';
import { action } from '../../store';
import { useHistory, Link, withRouter } from 'react-router-dom';
import { getCookie } from '../../utils/session';
import {
  registerUser,
  changePassword,
  updatePassword,
  loginUser,
  resetAllStatus,
  validatePasswordToken
} from '../../actions/Authentication-action';
// import { validatePasswordToken } from '../../saga/AuthenticationService';
// import history from '../../store/history';
import moment from 'moment';

const { TabPane } = Tabs;
const leftInput = { display: 'inline-block', width: 'calc(50% - 8px)' };
const rightInput = { display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' };
const fullInput = { display: 'inline-block', width: 'calc(100% - 8px)' }
const { Option } = Select;

const StartLearning = ({ guest, keyNumber, match: { params: { randomKey, email } }, isEmailVerified }) => {
  const userID = getCookie('user_id');
  let history = useHistory();
  const { loading } = guest;
  // const disabledDates = moment()
  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current > moment().add('years', -15);
  }

  useEffect(() => {
    if (guest && guest.isUserRegistered) {
      notification["success"]({
        message: "Registered Successfully"
      });
      // action(resetAllStatus());
    } else if (guest && guest.isUserLogin) {
      notification["success"]({
        message: "Login Successfully"
      });
      // action(resetAllStatus());
    } else if (guest && guest.isUserTokenValid) {
      notification["success"]({
        message: "Token Sent Successfully"
      });
    } else if (guest && guest.isPasswordUpdated) {
      notification["success"]({
        message: "Password Updated Successfully"
      });
    }
    action(resetAllStatus());
  }, [guest.isUserRegistered, guest.isUserLogin, guest.isUserTokenValid, guest.isPasswordUpdated])

  useEffect(() => {
    if (isEmailVerified) {
      notification["success"]({
        message: "Email Verified Successfully"
      });
    }
  }, [isEmailVerified]);
  useEffect(() => {
    if (randomKey) {
      action(
        validatePasswordToken({
          email: email,
          token: randomKey
        })
      );
    }
  }, [])

  useEffect(() => {
    if (guest.type === "error") {
      notification["error"]({
        message: guest.message
      });
      history.push("/start-learning/login")
    }

    if (guest.type === "success") {
      notification["success"]({
        message: guest.message
      });
    }
  }, [guest.message]);

  const onLoginFinish = values => {
    action(loginUser(values));
    // console.log('Success:', values);
  };

  const onLoginFinishFailed = errorInfo => {
    // console.log('Failed:', errorInfo);
  };

  const onRegisterFinish = values => {
    values.monthly_family_income = parseInt(values.monthly_family_income);
    values.dob = values.dob.format("YYYY-MM-DD");
    action(registerUser(values));
  };

  const onRegisterFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const updateStateUrl = (key) => {
    if (key === "1") history.push("/start-learning/login")
    else history.push("/start-learning/register")
  }

  const goToDashboard = () => {
    history.push('/user');
    history.go();
    history.go();
  }

  const onChangePasswordFinish = (values) => {
    action(changePassword(values));
  }

  const onChangePasswordFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  }

  const onUpdatePasswordFinish = (values) => {
    action(updatePassword({
      email: email,
      token: randomKey,
      pasword: values.password
    }));
  }

  const onUpdatePasswordFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  }

  return (
    <div className="card-container">
      {
        !!userID ? (
          <Row align="middle">
            <Col span={24} style={{ textAlign: 'center' }}>
              {/* <Link to="/user" type="primary">Go To Dashboard</Link> */}
              <Button onClick={goToDashboard} type="primary">Go To Dashboard</Button>
            </Col>
          </Row>
        ) : (
            (keyNumber === "3") ? (
              <Row>
                <Col span={8} offset={8}>
                  <Card>
                    <Form
                      name="forgrtPassword"
                      onFinish={onChangePasswordFinish}
                      onFinishFailed={onChangePasswordFailed}
                    >
                      <Form.Item
                        name="email"
                        rules={[{ type: "email", required: true, message: 'Please input your email!' }]}
                      >
                        <Input prefix={<UserOutlined />} placeholder="Enter Email" />
                      </Form.Item>

                      <Form.Item className="text-center">
                        <Button type="primary" htmlType="submit" className="submit-cta" loading={loading}>
                          Submit
                        </Button>
                        &nbsp; &nbsp;
                        <Link to="/start-learning/login">
                          <Button className="submit-cta">
                            Back
                          </Button>
                        </Link>
                      </Form.Item>
                    </Form>
                  </Card>

                </Col>
              </Row>
            ) : (
                (keyNumber === "4" && guest.isTokenValid) ? (
                  <Row>
                    <Col span={6} offset={9}>
                      <Card>
                        <h3 style={{ textAlign: "center" }}>Reset Password</h3>
                        <Form
                          name="forgrtPassword"
                          onFinish={onUpdatePasswordFinish}
                          onFinishFailed={onUpdatePasswordFailed}
                        >
                          <Form.Item
                            name="password"
                            rules={[
                              {
                                required: true,
                                message: 'Please input your password!',
                              },
                            ]}
                            hasFeedback
                          >
                            <Input.Password />
                          </Form.Item>

                          <Form.Item
                            name="confirmPassword"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                              {
                                required: true,
                                message: 'Please confirm your password!',
                              },
                              ({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                  }

                                  return Promise.reject('The two passwords that you entered do not match!');
                                },
                              }),
                            ]}
                          >
                            <Input.Password />
                          </Form.Item>

                          <Form.Item className="text-center">
                            <Button type="primary" htmlType="submit" className="submit-cta wd-140" loading={loading}>
                              Submit
                                                        </Button>
                                                        &nbsp; &nbsp;
                                                        <Link to="/start-learning/login">
                              <Button className="submit-cta wd-140">
                                Back
                                                            </Button>
                            </Link>

                          </Form.Item>
                        </Form>
                      </Card>

                    </Col>
                  </Row>
                ) : (
                    <Tabs type="card" activeKey={!guest.isTokenValid ? keyNumber : "1"} onChange={updateStateUrl}>
                      <TabPane tab="Sign In" className="login-tab" key="1">
                        <Form
                          name="login"
                          onFinish={onLoginFinish}
                          onFinishFailed={onLoginFinishFailed}
                        >
                          <Form.Item
                            name="email"
                            rules={[{ required: true, message: 'Please input your username!' }]}
                          >
                            <Input prefix={<UserOutlined />} placeholder="UserName" />
                          </Form.Item>

                          <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                          >
                            <Input.Password prefix={<KeyOutlined />} placeholder="Password" />
                          </Form.Item>

                          <Form.Item>
                            <Link to="/start-learning/forgot-password" className="login-form-forgot">
                              Forgot password
                                                        </Link>
                          </Form.Item>

                          <Form.Item className="text-center">
                            <Button type="primary" htmlType="submit" className="submit-cta" loading={loading}>
                              Log In
                                                        </Button>
                          </Form.Item>
                        </Form>
                      </TabPane>
                      <TabPane tab="Register Now" key="2">
                        <Form
                          name="register"
                          onFinish={onRegisterFinish}
                          onFinishFailed={onRegisterFailed}
                        >
                          <Form.Item>
                            <Input.Group compact>
                              <Form.Item
                                name="name"
                                rules={[{ required: true, message: 'Please input your username!' }]}
                                style={leftInput}
                              >
                                <Input placeholder="UserName" prefix={<UserOutlined />} />
                              </Form.Item>
                              <Form.Item
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                                style={rightInput}
                              >
                                <Input.Password prefix={<KeyOutlined />} placeholder="Password" />
                              </Form.Item>
                            </Input.Group>

                            <Input.Group >
                              <Form.Item
                                name="email"
                                rules={[{ required: true, message: 'Please input your email address!' }]}
                                style={fullInput}
                              >
                                <Input prefix={<MailOutlined />} placeholder="Email Address" />
                              </Form.Item>
                            </Input.Group>
                            <Input.Group compact>
                              <Form.Item
                                name="contact"
                                rules={[{ required: true, message: 'Please input your phone number!' }]}
                                style={leftInput}
                              >
                                <Input prefix={<MobileOutlined />} placeholder="Phone Number" />
                              </Form.Item>
                              <Form.Item
                                name="dob"
                                rules={[{ required: true, message: 'Please input your date of birth!' }]}
                                style={rightInput}
                              >
                                <DatePicker defaultPickerValue={moment().add('years', -15)} disabledDate={disabledDate} placeholder="DOB (YYYY-MM-DD)" disabledTime={(moment, partial) => console.log(moment, partial, "moment, partial")} />
                              </Form.Item>
                            </Input.Group>
                            <Input.Group compact>
                              <Form.Item
                                name="city"
                                rules={[{ required: true, message: 'Please input your city!' }]}
                                style={leftInput}
                              >
                                <Input placeholder="City" />
                              </Form.Item>
                              <Form.Item
                                name="region_type"
                                rules={[{ required: true, message: 'Please select your region!' }]}
                                style={rightInput}
                              >
                                <Select placeholder="Region">
                                  <Option value="Rural">Rural</Option>
                                  <Option value="Sub Urban">Sub Urban</Option>
                                  <Option value="Urban">Urban</Option>
                                </Select>
                              </Form.Item>
                            </Input.Group>
                            <Input.Group compact>
                              <Form.Item
                                name="educational_qualification"
                                rules={[{ required: true, message: 'Please input your education qualification!' }]}
                                style={leftInput}
                              >
                                <Input placeholder="Education" prefix={<UserOutlined />} />
                              </Form.Item>
                              <Form.Item
                                name="educational_institution"
                                rules={[{ required: true, message: 'Please input your education institution!' }]}
                                style={rightInput}
                              >
                                <Input placeholder="Institution" />
                              </Form.Item>
                            </Input.Group>
                            <Input.Group compact>
                              <Form.Item
                                name="fathers_employment"
                                rules={[{ required: true, message: "Please input your father's employement!" }]}
                                style={leftInput}
                              >
                                <Input placeholder="Father's Employment" />
                              </Form.Item>
                              <Form.Item
                                name="mothers_employment"
                                rules={[{ required: true, message: "Please input your mother's employment!" }]}
                                style={rightInput}
                              >
                                <Input placeholder="Mother's Employment" />
                              </Form.Item>
                            </Input.Group>
                            <Input.Group>
                              <Form.Item
                                name="monthly_family_income"
                                rules={[{ required: true, message: 'Please input your monthly family income!' }]}
                                style={fullInput}
                              >
                                <Input placeholder="Monthly Family Income" />
                              </Form.Item>
                            </Input.Group>
                            <Input.Group>
                              <Form.Item
                                name="no_of_siblings"
                                rules={[{ required: true, message: 'Please enter no. of siblings!' }]}
                                style={fullInput}
                              >
                                <Input prefix={<TeamOutlined />} placeholder="No. of Siblings" />
                              </Form.Item>
                            </Input.Group>
                            <Input.Group compact>
                              <Form.Item
                                name="religion"
                                rules={[{ required: true, message: 'Please input your religion!' }]}
                                style={leftInput}
                              >
                                <Input placeholder="Religion" />
                              </Form.Item>
                              <Form.Item
                                name="community"
                                rules={[{ required: true, message: 'Please select your community!' }]}
                                style={rightInput}
                              >
                                <Select placeholder="Community">
                                  <Option value="SC">SC</Option>
                                  <Option value="ST">ST</Option>
                                  <Option value="MBC">MBC</Option>
                                  <Option value="OBC">OBC</Option>
                                  <Option value="Other">Other</Option>
                                </Select>
                              </Form.Item>
                            </Input.Group>
                            <Input.Group>
                              <Form.Item
                                name="how_do_you_know"
                                rules={[{ required: true, message: 'Please tell us how do you know about GirlsleadGrils!' }]}
                                style={fullInput}
                              >
                                <Input prefix={<QuestionCircleOutlined />} placeholder="How do you know about GirlsLeadGirls" />
                              </Form.Item>
                            </Input.Group>

                          </Form.Item>


                          <Form.Item className="text-center">
                            <Button type="primary" htmlType="submit" className="submit-cta" >
                              Create Account
                            </Button>
                          </Form.Item>
                        </Form>
                      </TabPane>
                    </Tabs>
                  )
              )
          )
      }

    </div>
  )
}

export default memo(withRouter(StartLearning));