import { UserConstant } from "../constsant/UserConstant";
import { AuthConstant } from "../constsant/Auth-constant";
import { getCookie } from "../utils/session";

const initState = {
    token: "",
    message: "",
    user_id: "",
    loading: false,
    type: "",
    isTokenValid: false,
    chapterList: [],
    preAssessmentQuestions: [],
    inCompletePreAssessment: false,
    isPreAssessmentDone: false,
    correctPreAssessmentAnswers: 0,
    isPostAssessmentDone: false,
    correctPostAssessmentAnswers: 0,
    inCompletePostAssessment: false,
    postAssessmentQuestions: [],
    isAllChapterDone: false,
    chapterDetail: [],
    chapterStatus: [],
    profile: {},
    userId: getCookie('user_id') || undefined,

};

const UserReducer = (state = initState, { type, payload }) => {
    switch (type) {
        case AuthConstant.SET_USER_INFO:
            let profile = payload.data;
            return {
                ...state,
                profile: profile,
                loading: payload.loading
            }

        case UserConstant.CHAPTER_LIST:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading,
                chapterList: payload.chapters,
            };
        case UserConstant.SET_CHAPTER_STATUS:
            let data = payload.data.sort((a, b) => b.sort_number > a.sort_number)
            let isPreAmtDone = () => {
                let preAssessment = data.find(chapter => chapter.key == "preAssessment");
                return (preAssessment && preAssessment.isDone) ? true : false
            }
            let isPostAmtDone = () => {
                let postAssessment = data.find(chapter => chapter.key == "postAssessment");
                return (postAssessment && postAssessment.isDone) ? true : false
            }
            let isAllChapterDone = () => {
                let allChapters = data.slice(1, data.length - 1)
                let completedChapters = allChapters.filter((chapter) => chapter.isPassed)
                return completedChapters && allChapters.length == completedChapters.length
            }
            return {
                ...state,
                message: payload.message,
                loading: payload.loading,
                chapterStatus: data,
                isPreAssessmentDone: isPreAmtDone(),
                isAllChapterDone: isAllChapterDone(),
                isPostAssessmentDone: isPostAmtDone()
            };
        case UserConstant.SET_PRE_ASSESSMENT_QUESTIONS:
            let preAssessmentQuestions = []
            let inCompletePreAssessment = false;
            let isPreAssessmentDone = false;
            if (payload.questions && payload.questions.response && payload.questions.response.length) {
                payload.questions.response.map((question) => {
                    let choices = [];
                    question.assessment_choices.map((choice) => {
                        choices.push({
                            id: choice.id,
                            desc: choice.answer
                        })
                    })
                    let answer = undefined;
                    if (payload.answers && payload.answers.AnsweredQuestions && payload.answers.AnsweredQuestions.length) {
                        var answeredQuestion = payload.answers.AnsweredQuestions.find(qtn => qtn.question_id == question.assessment_question_id)
                        if (answeredQuestion) {
                            answer = answeredQuestion.score;
                        }
                        inCompletePreAssessment = !payload.answers.AnsweredAll;
                        isPreAssessmentDone = payload.answers.AnsweredAll;
                    }
                    preAssessmentQuestions.push({
                        id: question.assessment_question_id,
                        desc: question.assessment_question,
                        answer: answer,
                        choices: choices
                    })
                })
            }
            return {
                ...state,
                message: payload.message,
                loading: payload.loading,
                preAssessmentQuestions: preAssessmentQuestions,
                inCompletePreAssessment: inCompletePreAssessment,
                correctPreAssessmentAnswers: payload.answers.CorrectAnswers,
                isPreAssessmentDone: isPreAssessmentDone
            };
        case UserConstant.SET_POST_ASSESSMENT_QUESTIONS:
            let postAssessmentQuestions = []
            let inCompletePostAssessment = false;
            let isPostAssessmentDone = false;
            if (payload.questions && payload.questions.response && payload.questions.response.length) {
                payload.questions.response.map((question) => {
                    let choices = [];
                    question.assessment_choices.map((choice) => {
                        choices.push({
                            id: choice.id,
                            desc: choice.answer
                        })
                    })
                    let answer = undefined;
                    if (payload.answers && payload.answers.AnsweredQuestions && payload.answers.AnsweredQuestions.length) {
                        var answeredQuestion = payload.answers.AnsweredQuestions.find(qtn => qtn.question_id == question.assessment_question_id)
                        if (answeredQuestion) {
                            answer = answeredQuestion.score;
                        }
                        inCompletePostAssessment = !payload.answers.AnsweredAll;
                        isPostAssessmentDone = payload.answers.AnsweredAll;
                    }
                    postAssessmentQuestions.push({
                        id: question.assessment_question_id,
                        desc: question.assessment_question,
                        answer: answer,
                        choices: choices
                    })
                })
            }
            return {
                ...state,
                message: payload.message,
                loading: payload.loading,
                correctPostAssessmentAnswers: payload.answers.CorrectAnswers,
                postAssessmentQuestions: postAssessmentQuestions,
                inCompletePostAssessment: inCompletePostAssessment,
                isPostAssessmentDone: isPostAssessmentDone
            };
        case UserConstant.SET_MESSAGE:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading
            };
        case UserConstant.SET_CHAPTER_DETAIL:
            let chapterDetail = {
                questions: [],
                ...payload.info
            };
            if (payload.questions && payload.questions.length) {
                payload.questions.map((question) => {
                    let choices = [];
                    question.choices.map((choice) => {
                        choices.push({
                            id: choice.id,
                            desc: choice.answer
                        })
                    })
                    let answer = undefined;

                    chapterDetail.questions.push({
                        id: question.question_id,
                        desc: question.question,
                        answer: answer,
                        choices: choices
                    })
                })
            }
            return {
                ...state,
                chapterDetail: chapterDetail,
                loading: payload.loading
            }
        default:
            return state;
    }
}

export default UserReducer;