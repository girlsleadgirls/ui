import { GuestConstant } from "../constsant/GuestContant";
import { AuthConstant } from "../constsant/Auth-constant";
import { getCookie } from "../utils/session";

const initState = {
  token: "",
  message: "",
  user_id: getCookie('user_id') || "",
  loading: false,
  type: "",
  isTokenValid: false,
  chapterList: [],
  isUserRegistered: false,
  isUserLogin: false,
  isUserTokenValid: false,
  isPasswordUpdated: false,
  isEmailVerified: false,
  isEnquirySubmitted: false,
  isErrorAvailable: false,
};

const GuestReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case AuthConstant.LOGIN:
      return {
        ...state,
        user_id: payload.user_id,
        message: payload.message,
        loading: payload.loading,
        type: payload.type,
      };
    case GuestConstant.REGISTER:
      return {
        ...state,
        message: payload.message,
        loading: payload.loading,
        type: payload.type,
      };
    case GuestConstant.SET_MESSAGE:
      return {
        ...state,
        message: payload.message,
        loading: payload.loading,
        type: payload.type,
        isTokenValid: payload.isTokenValid
      };
    case GuestConstant.CHANGE_PASSWORD:
      return {
        ...state,
        message: payload.message,
        loading: payload.loading,
        type: payload.type,
      };

    case GuestConstant.CHAPTER_LIST:
      return {
        ...state,
        message: payload.message,
        loading: payload.loading,
        chapterList: payload.chapters,
      };
    case AuthConstant.UPDATE_REGISTER_STATUS:
      return {
        ...state,
        [payload.prop]: payload.status,
        message: (payload.message || "")
      };
    case AuthConstant.ALL_RESET_STATUS:
      return {
        ...state,
        isUserRegistered: false,
        isUserLogin: false,
        isUserTokenValid: false,
        isPasswordUpdated: false,
        isEmailVerified: false,
        isEnquirySubmitted: false,
        isErrorAvailable: false,
        message: ""
      }
    default:
      return state;
  }
}

export default GuestReducer;