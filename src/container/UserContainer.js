import React, { useEffect } from 'react';
import Dashboard from '../component/user/Dashboard'
import Profile from '../component/user/Profile'
import Lesson from '../component/user/Lesson'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import UserLayout from '../component/layout/UserLayout'
import Enquiry from '../component/enquiry';
import { connect } from 'react-redux';
import { getUserInfo } from '../actions/Authentication-action.js';
import { action } from '../store';

const UserContainer = ({ userDetail }) => {
  useEffect(() => {
    action(getUserInfo());
  }, [])
  return (
    <Router basename="/user">
      <UserLayout profile={userDetail.profile} >
        <Switch>
          <Route path="/profile">
            <Profile userDetail={userDetail.profile} />
          </Route>
          <Route path="/lessons">
            <Lesson />
          </Route>
          <Route path="/query">
            <Enquiry />
          </Route>
          <Route path={["/app", "/"]}>
            <Dashboard profile={userDetail.profile} chapters={userDetail.chapterStatus} userId={userDetail.userId} isPreAssessmentDone={userDetail.isPreAssessmentDone} isAllChapterDone={userDetail.isAllChapterDone} isPostAssessmentDone={userDetail.isPostAssessmentDone} loading={userDetail.loading} />
          </Route>
        </Switch>
      </UserLayout>
    </Router>
  )
}
const mapStateToProps = (state) => {
  const { UserReducer } = state;
  return { userDetail: UserReducer }
}

export default connect(mapStateToProps)(UserContainer)
