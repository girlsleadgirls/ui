import React from 'react'
import Dashboard from '../component/dashboard'
import StartLearning from '../component/startLearning';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import GuestLayout from '../component/layout/GuestLayout';
import Enquiry from '../component/enquiry';
import AboutGirlsLeadGirls from '../component/about-girls-lead-girls';
import { connect } from 'react-redux';
import EmailVerification from '../component/email-verification';
import { useEffect } from 'react';
import { notification } from 'antd';
import { action } from '../store';
import { resetAllStatus } from '../actions/Authentication-action';

const GuestContainer = ({ guest }) => {
  const {
    message,
    isErrorAvailable,
    isEmailVerified,
    isUserRegistered,
    isPasswordUpdated,
    isUserTokenValid,
  } = guest;

  useEffect(() => {
    if (isErrorAvailable) {
      notification["error"]({
        message
      });
      action(resetAllStatus());
    }
  }, [isErrorAvailable])
  return (
    <Router basename="/">
      <GuestLayout>
        <Switch>
          <Route exact path="/about-girls-lead-girls">
            <AboutGirlsLeadGirls />
          </Route>
          <Route exact path="/start-learning/login">
            <StartLearning guest={guest} keyNumber="1" isEmailVerified={isEmailVerified} />
          </Route>
          <Route exact path="/start-learning/register" render={() => {
            return isUserRegistered ? <Redirect to="/start-learning/login" /> : <StartLearning guest={guest} keyNumber="2" />
          }} />
          <Route exact path="/start-learning/forgot-password" render={() => {
            return isUserTokenValid ? <Redirect to="/start-learning/login" /> : <StartLearning guest={guest} keyNumber="3" />
          }} />

          <Route exact path="/change-password/:randomKey/:email" render={() => {
            return isPasswordUpdated ? <Redirect to="/start-learning/login" /> : <StartLearning guest={guest} keyNumber="4" />
          }} />

          <Route exact path="/email-verification/:token" render={() => {
            return isEmailVerified ? <Redirect to="/start-learning/login" /> : <EmailVerification isEmailVerified={guest.isEmailVerified} loading={guest.loading} />
          }} />
          <Route exact path="/enquiry">
            <Enquiry />
          </Route>
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </GuestLayout>
    </Router>
  )
}

const mapStateToProps = (state) => {
  const { GuestReducer } = state;
  return { guest: GuestReducer }
}

export default connect(mapStateToProps)(GuestContainer)