import React from 'react'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import GuestContainer from './GuestContainer';
import UserContainer from './UserContainer';
import history from '../store/history';
import { connect } from 'react-redux';

const AppContainer = ({ guest }) => {
  console.log(guest);
  return (
    <Router history={history}>
      <Switch>
        <Route path="/user" render={() => !guest.user_id ? <Redirect to='/' /> : <UserContainer />} />
        <Route path="/" render={() => guest.user_id ? <Redirect to='/user' /> : <GuestContainer />} />
      </Switch>
    </Router>
  )
}

const mapStateToProps = ({ GuestReducer }) => {
  return { guest: GuestReducer }
}

export default connect(mapStateToProps)(AppContainer)