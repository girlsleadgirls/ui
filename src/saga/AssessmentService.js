import { put, call } from 'redux-saga/effects'
import { UserConstant } from "../constsant/UserConstant";
import { getApiCall, postApiCall } from '../api/config';

export const getPreAssessmentQuestions = function* (payload) {
    try {
        yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
        const { data } = yield call(() => getApiCall("api/FetchAssessmentQuestion.php"));
        let url = "api/FetchAnsweredQuestion.php?id=" + payload.payload.userId + "&type=pre";
        const answerData = yield call(() => getApiCall(url));
        yield put({ type: UserConstant.SET_PRE_ASSESSMENT_QUESTIONS, payload: { questions: data, answers: answerData.data, message: "Fetched Assessment Questions Successfully", type: "success", loading: false } });
    } catch (error) {
        yield put({ type: 'SET_MESSAGE', payload: { message: error, type: "error", loading: false } });
        console.log(error);
    }
}

export const getPostAssessmentQuestions = function* (payload) {
    try {
        yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
        const { data } = yield call(() => getApiCall("api/FetchAssessmentQuestion.php"));
        let url = "api/FetchAnsweredQuestion.php?id=" + payload.payload.userId + "&type=post";
        const answerData = yield call(() => getApiCall(url));
        yield put({ type: UserConstant.SET_POST_ASSESSMENT_QUESTIONS, payload: { questions: data, answers: answerData.data, message: "Fetched Assessment Questions Successfully", type: "success", loading: false } });
    } catch (error) {
        yield put({ type: 'SET_MESSAGE', payload: { message: error, type: "error", loading: false } });
        console.log(error);
    }
}
export const insertPreAssessmentQuestions = function* ({ payload }) {
    try {
        yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
        const { data: { status } } = yield call(() => postApiCall("api/insertAssessmentScore.php", { ...payload }));
        if (status) {
            // yield getPreAssessmentQuestions()
            yield put({ type: 'SET_MESSAGE', payload: { message: "", loading: false } });
        } else {
            throw "Please Enter Valid data";
        }
    } catch (error) {

    }
}
export const insertPostAssessmentQuestions = function* ({ payload }) {
    try {
        yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
        const { data: { status } } = yield call(() => postApiCall("api/insertAssessmentScore.php", { ...payload }));
        if (status) {
            // yield getPreAssessmentQuestions()
            yield put({ type: 'SET_MESSAGE', payload: { message: "", loading: false } });
        } else {
            throw "Please Enter Valid data";
        }
    } catch (error) {

    }
}
