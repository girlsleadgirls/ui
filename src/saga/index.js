import { takeLatest } from 'redux-saga/effects'
import { authenticateUser, registerUser, changePassword, validateToken, updatePassword, updateUser, resetAllStatus, getUserInfo, saveFeedback, validatePasswordToken } from './AuthenticationService';
import { getChapterDetail, getChapterList, insertChapterQuestions, getChapterStatus, insertVideoTime } from './ChapterService';
import { getPreAssessmentQuestions, insertPreAssessmentQuestions, getPostAssessmentQuestions, insertPostAssessmentQuestions } from './AssessmentService';
import { GuestConstant } from '../constsant/GuestContant';
import { UserConstant } from '../constsant/UserConstant';
import { AuthConstant } from "../constsant/Auth-constant";
import { insertEnquiryForm } from './CommonService';


export default function* rootSaga() {
  yield takeLatest('AUTH', authenticateUser);
  yield takeLatest('REGISTER_USER', registerUser);
  yield takeLatest(AuthConstant.UPDATE_USER, updateUser);
  yield takeLatest(AuthConstant.USER_INFO, getUserInfo);
  yield takeLatest(AuthConstant.SAVE_FEEDBACK, saveFeedback);
  yield takeLatest('CHANGE_PASSWORD_SAGA', changePassword);
  yield takeLatest('VALIDATE_PASSWORD_TOKEN', validatePasswordToken);
  yield takeLatest('VALIDATE_TOKEN', validateToken);
  yield takeLatest('UPDATE_PASSWORD', updatePassword);
  yield takeLatest(GuestConstant.GET_CHAPTER, getChapterList);
  yield takeLatest(UserConstant.GET_CHAPTER, getChapterList);
  yield takeLatest(UserConstant.INSERT_CHAPTERS_QUESTIONS, insertChapterQuestions);
  yield takeLatest(UserConstant.GET_CHAPTER_STATUS, getChapterStatus);
  yield takeLatest(UserConstant.INSERT_VIDEO_TIME, insertVideoTime);
  yield takeLatest(UserConstant.PRE_ASSESSMENT_QUESTIONS, getPreAssessmentQuestions);
  yield takeLatest(UserConstant.INSERT_PRE_ASSESSMENT_QUESTIONS, insertPreAssessmentQuestions);
  yield takeLatest(UserConstant.POST_ASSESSMENT_QUESTIONS, getPostAssessmentQuestions);
  yield takeLatest(UserConstant.INSERT_POST_ASSESSMENT_QUESTIONS, insertPostAssessmentQuestions);
  yield takeLatest(UserConstant.GET_CHAPTER_DETAIL, getChapterDetail);
  yield takeLatest(AuthConstant.ALL_STATUS_RESET, resetAllStatus);
  yield takeLatest(GuestConstant.INSERT_ENQUIRY, insertEnquiryForm);
}
// export default rootSaga;