import { put, call } from 'redux-saga/effects'
import { postApiCall, getApiCall } from '../api/config';
import { AuthConstant } from '../constsant/Auth-constant';
import { setCookie, getCookie } from '../utils/session';

const authenticateUser = function* ({ payload }) {
  try {

    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isUserLogin" } });
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "", user_id: "", loading: false } });

    const { data: { user_id, status, message } } = yield call(() => postApiCall("api/Login.php", { ...payload }));
    if (status == 200) {
      setCookie({ index: 'user_id', value: user_id });
      yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isUserLogin" } });
      yield put({ type: AuthConstant.LOGIN, payload: { user_id, message: "Login Successfully", type: "success", loading: false } });
    } else {
      throw message;
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isUserLogin", message: error.message } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: (error.message || error) } });
    // yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: error, type: "error", loading: false } });
    console.log(error.message);
  }
}

const registerUser = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isUserRegistered" } });
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "", loading: false } });

    const { data: { status, message } } = yield call(() => postApiCall("api/Register.php", { ...payload }));

    if (status == 200) {
      yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isUserRegistered" } });
      yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "Registered Successfully", type: "success", loading: false } });
    } else {
      throw message;
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isUserRegistered" } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: (error.message || error) } });
    // yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: error, type: "error", loading: false } });
    console.log(error.message);
  }
}
const updateUser = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "", loading: false } });

    const { data: { status } } = yield call(() => postApiCall("api/updateProfile.php", { ...payload }));

    if (status) {
      yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "Profile updated Successfully", type: "success", loading: false } });
    } else {
      throw "Please Enter Valid data";
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { message: error, type: "error", loading: false } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: error.message } });
    console.log(error.message);
  }
}

const changePassword = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isUserTokenValid" } });
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "", loading: false } });

    const { data: { status } } = yield call(() => postApiCall("api/GeneratePasswordToken.php", { ...payload }));

    if (status) {
      yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isUserTokenValid" } });
      // yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "Mail Sent Successfully", type: "success", loading: false } });
    } else {
      throw "Please Enter Valid data";
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isUserTokenValid" } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: error.message } });
    // yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: error, type: "error", loading: false } });
    console.log(error.message);
  }
}

const validatePasswordToken = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isTokenValid" } });
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { isTokenValid: false, loading: false } });

    const { data: { status } } = yield call(() => postApiCall("api/ValidateToken.php", { ...payload }));

    if (status) {
      yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isTokenValid" } });
    } else {
      throw "Token Expired";
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { isTokenValid: false, message: "Token Expired", type: "error", loading: false } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: error.message } });
    console.log(error.message);
  }
}

const validateToken = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isEmailVerified" } });
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { isTokenValid: false, loading: false } });

    const { data: { status } } = yield call(() => postApiCall("api/Token.php", { ...payload }));

    if (status) {
      yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isEmailVerified" } });
    } else {
      throw "Token Expired";
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isEmailVerified" } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: error.message } });
    // yield put({ type: AuthConstant.SET_MESSAGE, payload: { isTokenValid: false, message: "Token Expired", type: "error", loading: false } });
    console.log(error.message);
  }
}

const updatePassword = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isPasswordUpdated" } });
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "", loading: false } });

    const { data: { status } } = yield call(() => postApiCall("api/ChangePassword.php", { ...payload }));

    if (status) {
      yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isPasswordUpdated" } });
      // yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "Password Updated Successfully", type: "success", loading: false } });
    } else {
      throw "Something went worng";
    }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: false, prop: "isPasswordUpdated" } });
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { status: true, prop: "isErrorAvailable", message: error.message } });
    // yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "Something went worng", type: "error", loading: false } });
    console.log(error.message);
  }
}

const resetAllStatus = function* () {
  yield put({ type: AuthConstant.ALL_RESET_STATUS });
}
const getUserInfo = function* () {
  try {
    yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
    let id = getCookie('user_id');
    const { data } = yield call(() => getApiCall("api/getUser.php?id=" + id));

    yield put({ type: AuthConstant.SET_USER_INFO, payload: { data: data, message: "Chapters Fetched Successfully", type: "success", loading: false } });
  } catch (error) {

    yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: false } });
  }
}
const saveFeedback = function* (payload) {
  try {
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: "", loading: true } });

    const { data } = yield call(() => postApiCall("api/Feedback.php", { ...payload.payload.data }));
    yield put({ type: AuthConstant.SET_USER_INFO, payload: { data: data, message: "Chapters Fetched Successfully", type: "success", loading: false } });
    if (data) {
      // payload.payload.callback()
    }
  } catch (error) {
    yield put({ type: AuthConstant.SET_MESSAGE, payload: { message: error, type: "error", loading: false } });
    console.log(error);
  }
}
export { authenticateUser, registerUser, changePassword, validateToken, updatePassword, updateUser, resetAllStatus, getUserInfo, saveFeedback, validatePasswordToken };