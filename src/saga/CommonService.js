import { put, call } from 'redux-saga/effects'
import { GuestConstant } from "../constsant/GuestContant";
import { postApiCall, getApiCall } from '../api/config';
import { AuthConstant } from '../constsant/Auth-constant';

export const insertEnquiryForm = function* ({ payload }) {
  try {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { prop: 'isEnquirySubmitted', status: false, loading: true } });

    const { data } = yield call(() => postApiCall("api/ContactUs.php", { ...payload }));

    // if (status) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { prop: 'isEnquirySubmitted', status: true, message: "Submitted Successfully", type: "success", loading: false } });
    // } else {
    //   throw "Something went wrong";
    // }
  } catch (error) {
    yield put({ type: AuthConstant.UPDATE_REGISTER_STATUS, payload: { prop: 'isEnquirySubmitted', status: false, loading: true } });
  }
}
