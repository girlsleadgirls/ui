import { put, call } from 'redux-saga/effects'
import { GuestConstant } from "../constsant/GuestContant";
import { UserConstant } from "../constsant/UserConstant";
import { postApiCall, getApiCall } from '../api/config';
import { getCookie } from '../utils/session';

export const getChapterList = function* () {
  try {
    yield put({ type: GuestConstant.CHAPTER_LIST, payload: { chapters: [], loading: true } });

    const { data } = yield call(() => getApiCall("api/getChapterList.php"));

    console.log(data, "data");
    // if (status) {
    yield put({ type: GuestConstant.CHAPTER_LIST, payload: { chapters: data, message: "Chapters Fetched Successfully", type: "success", loading: false } });
    // } else {
    //   throw "Invalid credential";
    // }
  } catch (error) {

    yield put({ type: 'SET_MESSAGE', payload: { message: error, type: "error", loading: false } });
  }
}

export const getChapterDetail = function* (payload) {
  try {
    yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
    let url = "api/getChapterDetail.php?chapter_id=" + payload.payload.chapter_id + "&user_id=" + payload.payload.user_id;
    const { data } = yield call(() => getApiCall(url));
    yield put({ type: UserConstant.SET_CHAPTER_DETAIL, payload: { questions: data.response, info: data.chapter, message: "Chapter Detail Fetched Successfully", type: "success", loading: false } });

  } catch (error) {

  }
}

// export const insertChapterQuestions = function* (payload) {
//   try {
//     yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
//     const { data: { status } } = yield call(() => postApiCall("api/InsertScore.php", { scores: payload.payload.questions }));
//     if (status) {
//       // yield getPreAssessmentQuestions()
//       payload.payload.callback()
//       yield put({ type: 'SET_MESSAGE', payload: { message: "", loading: false } });
//     } else {
//       throw "Please Enter Valid data";
//     }
//   }
//   catch (error) {

//   }
// }

export const insertChapterQuestions = function* (payload) {
  try {
    yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
    const { data: { status } } = yield call(() => postApiCall("api/InsertChapterScore.php", { ...payload.payload }));
    if (status) {
      // yield getPreAssessmentQuestions()
      yield put({ type: 'SET_MESSAGE', payload: { message: "", loading: false } });
    } else {
      throw "Please Enter Valid data";
    }
  }
  catch (error) {

  }
}

export const getChapterStatus = function* () {
  try {
    yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: true } });
    let url = "api/getChapterStatus.php?id=" + getCookie("user_id");
    const { data } = yield call(() => getApiCall(url));
    yield put({ type: UserConstant.SET_CHAPTER_STATUS, payload: { data: data, message: "Chapter Status Fetched Successfully", type: "success", loading: false } });
  } catch (error) {
    yield put({ type: 'SET_MESSAGE', payload: { message: error, type: "error", loading: false } });
    console.log(error);
  }
}

export const insertVideoTime = function* (payload) {
  try {
    yield put({ type: 'SET_MESSAGE', payload: { message: "", user_id: "", loading: false } });
    const { data: { status } } = yield call(() => postApiCall("api/insertVideoTime.php", { ...payload.payload }));
    if (status) {
      // yield getPreAssessmentQuestions()
      yield put({ type: 'SET_MESSAGE', payload: { message: "", loading: false } });
    } else {
      throw "Please Enter Valid data";
    }
  }
  catch (error) {

  }
}
