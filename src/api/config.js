// import axios from 'react-axios';
import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'http://localhost/',
  timeout: 2000,
  // "Authorization": `JWT ${token}`
});

const postApiCall = (url, data) => {
  return axiosInstance.post(url, data)
    .then(response => response)
    .catch(err => {
      throw err;
    });
}

const getApiCall = (url, data) => {
  return axiosInstance.get(url, data)
    .then(response => response)
    .catch(error => {
      console.error('There was an error!', error);
    });
}

const putApiCall = (url, data) => {
  return axiosInstance.put(url, data)
    .then(response => response)
    .catch(error => {
      console.error('There was an error!', error);
    });
}


export { getApiCall, putApiCall, postApiCall };