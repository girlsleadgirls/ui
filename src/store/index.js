import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import GuestReducer from '../reducer/GuestReducer'
import UserReducer from '../reducer/UserReducer'

import rootSaga from '../saga';

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  combineReducers({ GuestReducer, UserReducer }),
  compose(applyMiddleware(sagaMiddleware), window.__REDUX_DEVTOOLS_EXTENSION__()),
)
sagaMiddleware.run(rootSaga)

export const action = type => store.dispatch({ ...type })

export default store;