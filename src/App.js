import React from 'react';
import 'antd/dist/antd.css';
import './App.css';
import AppContainer from './container/AppContainer';

function App() {
  return (
    // <div className="App">
    <AppContainer />
    // </div>
  );
}

export default App;
