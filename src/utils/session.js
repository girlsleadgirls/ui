import cookie from 'react-cookies'

const getCookie = (prop) => {
  return cookie.load(prop)
}

const setCookie = ({ index, value, url = '/' }) => {
  cookie.save(index, value, { path: url })
}

const removeCookie = (index) => {
  cookie.remove(index, { path: '/' })
}

export { getCookie, setCookie, removeCookie }