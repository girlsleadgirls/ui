import { UserConstant } from "../constsant/UserConstant";

export const getChapters = () => ({
    type: UserConstant.GET_CHAPTER,
});

export const getChapterDetail = (payload) => ({
    type: UserConstant.GET_CHAPTER_DETAIL,
    payload
})

export const insertChapterQuestions = (payload) => ({
    type: UserConstant.INSERT_CHAPTERS_QUESTIONS,
    payload
})
export const getChapterStatus = () => ({
    type: UserConstant.GET_CHAPTER_STATUS
})
export const insertVideoTime = (payload) => ({
    type: UserConstant.INSERT_VIDEO_TIME,
    payload
})