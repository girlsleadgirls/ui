import { GuestConstant } from "../constsant/GuestContant";

export const getChapters = () => ({
  type: GuestConstant.GET_CHAPTER,
});

export const insertEnquiry = (payload) => ({
  type: GuestConstant.INSERT_ENQUIRY,
  payload: payload.user
});