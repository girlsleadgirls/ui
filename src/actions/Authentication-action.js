import { AuthConstant } from "../constsant/Auth-constant";

export const validateToken = ({ token }) => ({
  type: AuthConstant.VALIDATE_TOKEN,
  payload: { token },
});

export const validatePasswordToken = ({ email, token }) => ({
  type: AuthConstant.VALIDATE_PASSWORD_TOKEN,
  payload: { email, token },
});

export const registerUser = (payload) => ({
  type: AuthConstant.REGISTER_USER,
  payload: payload,
});

export const changePassword = (payload) => ({
  type: AuthConstant.CHANGE_PASSWORD_SAGA,
  payload: payload,
});

export const updatePassword = ({ email, token, pasword }) => ({
  type: AuthConstant.UPDATE_PASSWORD,
  payload: { email, token, pasword },
});

export const loginUser = (payload) => ({
  type: AuthConstant.AUTH,
  payload,
});

export const updateUser = (payload) => ({
  type: AuthConstant.UPDATE_USER,
  payload: payload,
});
export const resetAllStatus = () => ({
  type: AuthConstant.ALL_STATUS_RESET,
})

export const getUserInfo = () => ({
  type: AuthConstant.USER_INFO
})

export const saveFeedback = (payload) => ({
  type: AuthConstant.SAVE_FEEDBACK,
  payload
})