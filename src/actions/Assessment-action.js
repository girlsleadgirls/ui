import { UserConstant } from "../constsant/UserConstant";

export const getPreAssessmentQuestions = (payload) => ({
    type: UserConstant.PRE_ASSESSMENT_QUESTIONS,
    payload
})
export const insertPreAssessmentQuestions = (payload) => ({
    type: UserConstant.INSERT_PRE_ASSESSMENT_QUESTIONS,
    payload
})
export const getPostAssessmentQuestions = (payload) => ({
    type: UserConstant.POST_ASSESSMENT_QUESTIONS,
    payload
})
export const insertPostAssessmentQuestions = (payload) => ({
    type: UserConstant.INSERT_POST_ASSESSMENT_QUESTIONS,
    payload
})